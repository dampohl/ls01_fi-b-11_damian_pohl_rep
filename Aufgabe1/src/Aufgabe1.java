public class Aufgabe1 {

	public static void main(String[] args) {

		//Konsolenausgabe �bung 1
		//Aufgabe 1
		System.out.print("Das ist ein Beispielsatz. ");
		System.out.print("Ein Beispielsatz ist das.\n");
		
		System.out.print("Das ist ein \"Beispielsatz\".\n");
		System.out.print("Ein Beispielsatz ist das.\n");
		
		System.out.println("\n\n");
		
		//Aufgabe 2
		String stern1 = "*";
		String stern3 = "***";
		String stern5 = "*****";
		String stern7 = "*******";
		String stern9 = "*********";
		String stern11 = "***********";
		String stern13 = "*************";
		
		System.out.printf( "%7s\n", stern1 );
		System.out.printf( "%8s\n", stern3 );
		System.out.printf( "%9s\n", stern5 );
		System.out.printf( "%10s\n", stern7 );
		System.out.printf( "%11s\n", stern9 );
		System.out.printf( "%12s\n", stern11 );
		System.out.printf( "%s\n", stern13 );
		System.out.printf( "%8s\n", stern3 );
		System.out.printf( "%8s\n", stern3 );
		
		System.out.println("\n\n");
		
		//Aufgabe 3
		
		double zahl1 = 22.4234234;
		double zahl2 = 111.2222;
		double zahl3 = 4.0;
		double zahl4 = 1000000.551;
		double zahl5 = 97.34;
		
		System.out.printf("%.2f\n", zahl1 );
		System.out.printf("%.2f\n", zahl2 );
		System.out.printf("%.2f\n", zahl3 );
		System.out.printf("%.2f\n", zahl4 );
		System.out.printf("%.2f\n", zahl5 );
		
	}
}
