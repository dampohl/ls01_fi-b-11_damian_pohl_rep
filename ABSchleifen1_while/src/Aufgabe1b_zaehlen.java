import java.util.Scanner;

public class Aufgabe1b_zaehlen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	// b), Ausgabe nat. Zahlen n bis 1
		
		// Variablen
		int n;
		
		// Eingabe n
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Bitte geben Sie eine natürliche Zahl n ein: ");
		n = myScanner.nextInt();

		// Verarbeitung
		 while(n != 0)
		 {
			//Ausgabe
			 System.out.print(n + " ");
			 n--;
		 }
		
	}
}
