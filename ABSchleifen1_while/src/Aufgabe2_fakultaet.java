import java.util.Scanner;

public class Aufgabe2_fakultaet {

	public static void main(String[] args) {

		//Variablen
		int n = 999;
		long faktor1 = 1;
		long faktor2 = 2;
		long ergebnis = 0;
		
		// Eingabe n
		while(n > 20)
		{
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Bitte geben Sie eine nat�rliche Zahl n ein, welche kleiner als 20 ist: ");
		n = myScanner.nextInt();
		}
		
		//Verarbeitung
		while(faktor2 <= n)
		{
			ergebnis = faktor1*faktor2;
			faktor1=ergebnis;
			faktor2=faktor2+1;
		}
		System.out.println("Die Fakult�t von 'n = " + n + "' ist " + ergebnis);
	}
}
