/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    byte anzahlPlaneten = 8 ;
    
    // Anzahl der Sterne in unserer Milchstra�e
    long anzahlSterne = 1000000000 ;
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3645000 ;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
     int alterTage = 8760 ;
    
    // Wie viel wiegt das schwerste Tier der Welt?
     long gewichtBlauwal = 130000 ;
    // Schreiben Sie das Gewicht in Kilogramm auf!
     int gewichtKilogramm = 95 ;
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
    int flaecheGroessteLand = 17098242 ;
    
    // Wie gro� ist das kleinste Land der Erde?
    
    double flaecheKleinsteLand = 0.44 ;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    System.out.println("Anzahl der Einwohner in Berlin: " + bewohnerBerlin);
    System.out.println("Mein Alter in Tagen: " + alterTage);
    System.out.println("Gewicht eines Blauwals, dem schwersten Tier der Welt, in kg: " + gewichtBlauwal);
    System.out.println("Mein Gewichtin kg: " + gewichtKilogramm);
    System.out.println("Fl�che von Russland, dem gro��ten Land der Welt, in km�: " + flaecheGroessteLand);
    System.out.println("Fl�che des Vatikanstaats, dem kleinsten Land der Welt in km�: " + flaecheKleinsteLand);
    
    
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

