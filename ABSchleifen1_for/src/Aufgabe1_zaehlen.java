import java.util.Scanner;

public class Aufgabe1_zaehlen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	// a), Ausgabe nat. Zahlen 1 bis n
		
		// Variablen
		int n;
		int laufwert;
		
		// Eingabe n
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Bitte geben Sie eine natürliche Zahl n ein: ");
		n = myScanner.nextInt();

		// Verarbeitung
		for (laufwert = 1; laufwert <= n; laufwert++) {
		//Ausgabe
			System.out.print(laufwert +" ");
			
		}
	}
}
