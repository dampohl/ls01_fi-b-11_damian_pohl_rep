import java.util.Scanner;

public class Aufgabe1b_zaehlen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	// b), Ausgabe nat. Zahlen n bis 1
		
		// Variablen
		int n;
		int laufwert;
		
		// Eingabe n
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Bitte geben Sie eine natürliche Zahl n ein: ");
		n = myScanner.nextInt();

		// Verarbeitung
		for (laufwert = 1; laufwert <= n; n--) {
		
		//Ausgabe
			System.out.print(n +" ");
			
		}
	}
}
