public class konsolenausgabe2 {

	public static void main(String[] args) {

		//Konsolenausgabe �bung 2
		//Aufgabe 1
		System.out.println("Aufgabe 1:\n\n");
		
		String stern1 = "*    ";
		String stern2 = "    *";
		String sterndoppel = "**";
		
		System.out.printf( "%6s\n", sterndoppel );
		System.out.printf( "%s\n", stern1 + stern2 );
		System.out.printf( "%s\n", stern1 + stern2 );
		System.out.printf( "%6s\n", sterndoppel );
		
		System.out.println("\n\n");
		
		//Aufgabe 2
		System.out.println("Aufgabe 2:\n\n");
		
	String f0 = "0!";
	String f1 = "1!";
	String f2 = "2!";
	String f3 = "3!";
	String f4 = "4!";
	String f5 = "5!";
	
	String fak0 = "";
	String fak1 = "1";
	String fak2 = "1 * 2";
	String fak3 = "1 * 2 * 3";
	String fak4 = "1 * 2 * 3 * 4";
	String fak5 = "1 * 2 * 3 * 4 * 5";
	
	String e0 = "1";
	String e1 = "1";
	String e2 = "2";
	String e3 = "6";
	String e4 = "24";
	String e5 = "120";
	
	String equal = "=";
	
	System.out.printf( "%-4s", f0);
	System.out.printf( "%-2s", equal );
	System.out.printf( "%-18s", fak0);
	System.out.printf("%s", equal);
	System.out.printf("%5s", e0);
	System.out.print("\n");
	
	System.out.printf( "%-4s", f1);
	System.out.printf( "%-2s", equal );
	System.out.printf( "%-18s", fak1);
	System.out.printf("%s", equal);
	System.out.printf("%5s", e1);
	System.out.print("\n");
	
	System.out.printf( "%-4s", f2);
	System.out.printf( "%-2s", equal );
	System.out.printf( "%-18s", fak2);
	System.out.printf("%s", equal);
	System.out.printf("%5s", e2);
	System.out.printf("\n");
	
	System.out.printf( "%-4s", f3);
	System.out.printf( "%-2s", equal );
	System.out.printf( "%-18s", fak3);
	System.out.printf("%s", equal);
	System.out.printf("%5s", e3);
	System.out.printf("\n");
	
	System.out.printf( "%-4s", f4);
	System.out.printf( "%-2s", equal );
	System.out.printf( "%-18s", fak4);
	System.out.printf("%s", equal);
	System.out.printf("%5s", e4);
	System.out.printf("\n");
	
	System.out.printf( "%-4s", f5);
	System.out.printf( "%-2s", equal );
	System.out.printf( "%-18s", fak5);
	System.out.printf("%s", equal);
	System.out.printf("%5s", e5);
	System.out.printf("\n");
	
	System.out.print("\n");
	
	//Aufgabe 3
	System.out.println("Aufgabe 3:\n\n");
	
	int fahrenheit1 = -20;
	int fahrenheit2 = -10;
	int fahrenheit3 = 0;
	int fahrenheit4 = 20;
	int fahrenheit5 = 30;
	
	double Celsius1 = -28.8889;
	double Celsius2 = -23.3333;
	double Celsius3 = -17.7778;
	double Celsius4 = -6.6667;
	double Celsius5 = -1.1111;
			
	System.out.printf( "%-12s|%10.2f\n%-12s|%10.2f\n%-12s|%10.2f\n%-12s|%10.2f", fahrenheit1, Celsius1, fahrenheit2, Celsius2, fahrenheit3, Celsius3, fahrenheit4, Celsius4, fahrenheit5, Celsius5);
		
	}
}
